import { ref } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isshow = ref(false);
  const message = ref("");
  const timeout = ref(2000);
  const showMessage = (msg: string, tout: number = 2000) => {
    message.value = msg;
    isshow.value = true;
    timeout.value = tout;
  };
  const closeMessage = () => {
    message.value = "";
    isshow.value = false;
  };
  return { isshow, message, showMessage, closeMessage, timeout };
});
